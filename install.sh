#!/bin/bash

# Only works with gnome 3

# Install packages
echo "Update"
sudo apt-get update && sudo apt-get upgrade > /dev/null

echo "Installing packages"
sudo apt-get install vim tmux chromium minicom curl ruby dirmngr -y > /dev/null

# Set dark theme
echo "Setting dark theme"
SETTINGS=~/.config/gtk-3.0/settings.ini
if [ ! -f $SETTINGS ]; then
    echo -e "[Settings]\ngtk-application-prefer-dark-theme=1" > $SETTINGS
else
    echo "Settings already exits"
fi

# Install docker
echo "Installing docker"
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common -y > /dev/null
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/debian \
    $(lsb_release -cs) \
    stable"
sudo apt-get update -y > /dev/null
sudo apt-get install docker-ce -y > /dev/null
sudo groupadd docker
sudo usermod -aG docker $USER

# Change docker images path
echo "Change docker image path"
if [ -d /etc/docker ]; then
    sudo sh -c 'echo "{\n\t\"graph\": \"$HOME/docker-data\"\n}" > /etc/docker/daemon.json'
else
    echo "Docker not installed"
fi

# Install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Install yed
echo "Installing yed"
curl https://www.yworks.com/resources/yed/demo/yEd-3.18.2_with-JRE10_64-bit_setup.sh --output install_yed.sh
chmod +x install_yed.sh
./install_yed.sh
rm -rf install_yed.sh

# Install spotify
echo "Installing spotify"
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys A87FF9DF48BF1C90
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
sudo sh -c 'echo deb http://security.debian.org/debian-security wheezy/updates main >> /etc/apt/sources.list'
sudo apt-get update -y
sudo apt-get install libssl1.0.0 -y
sudo apt-get install spotify-client -y

# Copy rc files
cp -rv .* ~
rsync -av --progress .* ~/ --exclude='.git/*'

# Install vim plugins
echo "Install vim plugins"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall

# Install zsh
sudo apt-get install zsh -y
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
sed -i '/^plugins=(/a zsh-syntax-highlighting' ~/.zshrc
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
sed -i '/^plugins=(/a zsh-autosuggestions' ~/.zshrc

# Install fonts
sudo apt-get install fonts-powerline -y
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts

# Change zsh theme
echo "Change zsh theme"
sed -i 's/\(ZSH_THEME="\)\(.*\)\("\)/\1agnoster\3/' ~/.zshrc
echo "CHANGE YOUR TERMINAL FONT TO POWELINE FONT"

# Add shortcuts to zsh
echo "bindkey '^ ' autosuggest-accept" >> ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

echo "Better to reboot"
