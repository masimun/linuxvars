[ -f $PROFILEROOT/bash-common/.bashrc ] && . $PROFILEROOT/bash-common/.bashrc

export TERM=xterm-256color

PROMPT_DIRTRIM=2

CBLUE="\[\033[38;5;32m\]"
CGREEN="\[\033[38;5;46m\]"
CORANGE1="\[\033[38;5;202m\]"
CORANGE2="\[\033[38;5;214m\]"
CPURPLE="\[\033[38;5;128m\]"
CEND="\[$(tput sgr0)\]"
# PS1="[$CORANGE2\A$CEND][$CEND$CBLUE\u@\h$CEND][$CPURPLE\w$CEND]\\$ "

#########
# Test  #
#########

# A More Awesome PS1
# - only show username if its not the usual one
# - only show hostname if its not my local box (checks SSH_CONNECTION)
# - show root username in red
# - show current path, and smart truncate longer ones (>35 chars)
# - show git branch in brackets
# - show a red star for uncommitted git changes
# - show local hostname in green (192.*,10.*,::1)
# - show other hostnames in white
# - use nice colors overall

# Define Colors

txtblk="\e[0;30m" # Black - Regular
txtred="\e[0;31m" # Red
txtgrn="\e[0;32m" # Green
txtylw="\e[0;33m" # Yellow
txtblu="\e[0;34m" # Blue
txtpur="\e[0;35m" # Purple
txtcyn="\e[0;36m" # Cyan
txtwht="\e[0;37m" # White
bldblk="\e[1;30m" # Black - Bold
bldred="\e[1;31m" # Red
bldgrn="\e[1;32m" # Green
bldylw="\e[1;33m" # Yellow
bldblu="\e[1;34m" # Blue
bldpur="\e[1;35m" # Purple
bldcyn="\e[1;36m" # Cyan
bldwht="\e[1;37m" # White
unkblk="\e[4;30m" # Black - Underline
undred="\e[4;31m" # Red
undgrn="\e[4;32m" # Green
undylw="\e[4;33m" # Yellow
undblu="\e[4;34m" # Blue
undpur="\e[4;35m" # Purple
undcyn="\e[4;36m" # Cyan
undwht="\e[4;37m" # White
bakblk="\e[40m"   # Black - Background
bakred="\e[41m"   # Red
badgrn="\e[42m"   # Green
bakylw="\e[43m"   # Yellow
bakblu="\e[44m"   # Blue
bakpur="\e[45m"   # Purple
bakcyn="\e[46m"   # Cyan
bakwht="\e[47m"   # White
txtrst="\e[0m"    # Text Reset

# test for local hostname preference
if test -z "$SERVERNAME"; then
	SERVERNAME=`hostname -s`
fi

parse_git_branch() {
  # get current branch name
	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/[\1]/' -e 's/ *//'
}
parse_git_status(){
  # show * when there are uncommited changes
  git st -s 2>/dev/null|wc -l|sed -e 's/0//' -e 's/[0-9][0-9]*/*/' -e 's/ *//'
}
parse_user(){
  # only display username if it's different than my usual one
  if [[ -z $DEFAULT_USERNAME && $USER != $DEFAULT_USERNAME ]]; then echo -n $USER; fi
}
user_color(){
  if [ $USER = 'root' ]; then echo "$txtylw"; else echo "$txtred"; fi
}
parse_host(){
  # only display hostname if on a remote box via ssh
  if [ -n "$SSH_CONNECTION" ]; then echo -n "$SERVERNAME"; fi
}
host_color(){
  if [[ $SSH_CLIENT =~ ^(192\.168\.|\:\:1 |10\.) ]]; then echo "$txtgrn"; else echo "$txtwht"; fi
}
parse_pwd(){
  # 1) replace $HOME with ~
  # 2) replace ~/Sites with //
  # 3) truncate paths using two methods:
  #    - split on '/', or
  #    - use first/last chars
  #    ...and use whatever ends up being shortest
  ruby -e 'd=ENV["PWD"].gsub(%r{^#{ENV["HOME"]}},"~").gsub(%r{^~/Sites/},"//").gsub(%r{^/var/www/vhosts/},"//");p1=d[0,25]+"…"+d[-15,15].to_s;a=d.split("/");p2=a.first(4)+["…"]+a.last(2);p2=p2.join("/");puts (d.size>[p1.size,p2.size].min)?((p1.size<p2.size)?p1:p2):d'
}
# set term title bar without colors or git branch
if [ "$TERM" = "xterm-color" -o "$TERM" = "xterm" ]; then
	TITLE_BAR="\[\e]1;\$(parse_user)@\$(parse_host) \$(parse_pwd)\[\a\]"
fi
# set PS1 with everything
export PS1="[$CORANGE2\A$CEND] $TITLE_BAR\[$(user_color)\]$(parse_user)\[$bldgrn\]@\[$(host_color)\]$(parse_host) \[$undpur\]\$(parse_pwd) \[$txtrst\]\[$bldblk\]\$(parse_git_branch)\[$bldred\]\$(parse_git_status)\[$txtrst\] \$ "



#########
# ALIAS #
#########
alias rmr='rm -r'
alias isamag='ag -f --hidden --ignore-dir=.hg --ignore-dir=.git --ignore-dir=.svn --silent --ignore *.map --ignore *_Gen.cpp --ignore *.cmd --ignore *.mem --ignore *.link'
alias ls='/bin/ls --color=auto'
alias l='ls -1'
alias ll='ls -l'
alias la='ls -al'
alias lar='ls -alR'
alias cdr='change_dir_hg_root'
alias cd1='cd /repo/pdevloed/repo_1/sw/'
alias cd2='cd /repo/pdevloed/repo_2/sw/'
alias cd3='cd /repo/pdevloed/repo_3/'
alias cdu='cd ..'
alias cdh='cd $HOME'
alias cdp='cd -'
alias cdb='change_dir_board_x'
alias untar='tar -xvf'
alias todo='cat ~/TODO'
alias etodo='vim ~/TODO'

bind TAB:menu-complete


#############
# FUNCTIONS #
#############

if [ -f $HOME/scripts/bash_functions.sh ]
then
    . $HOME/scripts/bash_functions.sh
fi

if [ -f $HOME/scripts/create_exercise.sh ]
then
    . $HOME/scripts/create_exercise.sh
fi

mkcd () {
  mkdir "$1"
  cd "$1"
}
