"  .vimrc
" -----------------------------------------------------------------------------
" *****************************************************************************
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
"
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

Plugin 'bling/vim-bufferline'

Plugin 'easymotion/vim-easymotion'

"Plugin 'Yggdroot/indentLine'

Plugin 'triglav/vim-visual-increment'

Plugin 'jeffkreeftmeijer/vim-numbertoggle'

Plugin 'severin-lemaignan/vim-minimap'

Plugin 'joshdick/onedark.vim'

Plugin 'kergoth/vim-bitbake'

Plugin 'scrooloose/nerdtree'

Plugin 'c.vim'

Plugin 'scrooloose/syntastic'
" Plugin 'ntpeters/vim-better-whitespace'

" The following are examples of different formats supported.

" " All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" " To ignore plugin indent changes, instead use:
" "filetype plugin on
" "
" " Brief help
" " :PluginList       - lists configured plugins
" " :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" " :PluginSearch foo - searches for foo; append `!` to refresh local cache
" " :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this line

set exrc
" Number settings
set number
" set relativenumber

" Colorscheme
set background=dark

" Better file searching
set wildmenu

syntax enable

" colorscheme onedark
set colorcolumn=120
highlight ColorColumn ctermbg=235

filetype plugin indent on

" show existing tab with 4 spaces width
set tabstop=4

" " when indenting with '>', use 4 spaces width
set shiftwidth=4

" " On pressing tab, insert 4 spaces
"set expandtab
" On pressing tab, insert tab
set noexpandtab
set autoindent

set listchars=tab:>-,trail:~,extends:>,precedes:<
set list
autocmd FileType make setlocal noexpandtab
autocmd FileType dts setlocal noexpandtab

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
autocmd vimenter * NERDTree

" Highlight current line
"set cursorline

" Better searching
set hlsearch
set incsearch
set smartcase
"set ignorecase

" Ignore compiled files
set wildignore=*.o,*~,*.pyc

" Enable buffer switching without saving
set hidden

" Show the current command-key-combination at the right side in the status bar
set showcmd

let g:airline_powerline_fonts = 1
let g:airline_theme='minimalist'

" For regular expressions set magic on
set magic

" Show matching brackets when text inidicator is over them
set showmatch

" Enable folding
set foldenable

set scrolloff=8
"if exists('+colorcolumn')
    "set colorcolumn=80,100,120
"endif

" Set foldmethod to indent
" set foldmethod=indent

" Absolute numbers on focusloss/relative numbers on focusgain
" au FocusLost * :set number
" au FocusGained * :set relativenumber
" Enable list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

"let g:airline_section_b = '%{strftime("%c")}'
"let g:airline_section_y = 'BN: %{bufnr("%")}'

" Easymotion config
let g:EasyMotion_do_mapping = 0
nmap s <Plug>(easymotion-overwin-f2)
map <leader>j <Plug>(easymotion-j)
map <leader>k <Plug>(easymotion-k)

nnoremap <F6> :bp <CR>
nnoremap <F7> :bn <CR>
nnoremap <F9> :Explore <CR>

nnoremap <Tab> :bnext<CR>
nnoremap <S-Tab> :bprevious<CR>


map <C-n> :NERDTreeToggle<CR>

let g:airline_symbols = {}

" Remove trailing whitespace using <F5>
nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

au bufnewfile *.sh 0r ~/.vim/sh_header.temp

" Better whitespace
let g:strip_whitespace_on_save = 1

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

imap jj <Esc>
map <F10> :%!astyle --style=kr --indent=spaces=8 --indent-switches  --indent-classes --pad-oper --pad-header --unpad-paren --convert-tabs <CR>

" set colorcolumn=120
"set columns=120
"set secure

"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
